#!/usr/bin/env python
import rospy
from control_msgs.msg import FollowJointTrajectoryActionGoal
from control_msgs.msg import JointTrajectoryControllerState
from trajectory_msgs.msg import JointTrajectoryPoint
from std_msgs.msg import Empty
from std_msgs.msg import Header
from oct2py import Oct2Py as Octave
from sensor_msgs.msg import Joy
import numpy as np


class PathPlanner():
    joy_on = False # JOY ACTIVE FLAG
    joy_btns = Joy().buttons #DATA BLOCK FOR SAVING PRESSED BUTTONS
    joy_axes = Joy().axes #DATA BLOCK FOR SAVING PRESSED BUTTONS
    joint_states = JointTrajectoryControllerState() # CURRENT POSE/POSITION
    # Must have callback for msgs
    def state_callback(self,data):
        self.joint_states = data

    def empty_callback(self,data):
        rospy.loginfo("Empty message received")

    def joy_callback(self,data):
        if data.buttons[5] and not self.joy_on:
            self.joy_on = True
            rospy.loginfo('Joystick control ON')
        elif not data.buttons[5] and self.joy_on:
            self.joy_on = False
            rospy.loginfo('Joystick control OFF')
        self.joy_btns = data.buttons
        self.joy_axes = data.axes

    def joy_move_robot(self):
        dq = [0.0,0.0,0.0,0.0,0.0,0.0]
        if self.joy_btns[0] and not self.joy_btns[1] and not self.joy_btns[2]:
            dq[0] = self.joy_axes[0]*2*np.pi/180
            dq[1] = self.joy_axes[1]*2*np.pi/180
        elif not self.joy_btns[0] and self.joy_btns[1] and not self.joy_btns[2]:
            dq[2] = self.joy_axes[0]*2*np.pi/180
            dq[3] = self.joy_axes[1]*2*np.pi/180
        elif not self.joy_btns[0] and not self.joy_btns[1] and self.joy_btns[2]:
            dq[4] = self.joy_axes[0]*2*np.pi/180
            dq[5] = self.joy_axes[1]*2*np.pi/180
        if max(dq) >= np.pi/180 or min(dq)<=-np.pi/180:
            h = Header()
            h.stamp = rospy.Time.now()
            joy_traj_vector = FollowJointTrajectoryActionGoal()
            joy_traj_vector.header = h
            joy_traj_vector.goal.trajectory.joint_names = self.joint_states.joint_names
            joy_traj_vector.goal_id.stamp = h.stamp
            joy_traj_vector.goal_id.id = 'rosPathPlanner' + str(h.stamp.secs)
            h2 = Header()
            h2.stamp.secs = h.stamp.secs +1
            h2.stamp.nsecs = h.stamp.nsecs + 3e8
            joy_traj_vector.goal.trajectory.header.stamp = h2.stamp
            point = JointTrajectoryPoint()
            #Current position
            point.positions = self.joint_states.desired.positions
            point.time_from_start.nsecs = 0
            point.time_from_start.secs = 0
            joy_traj_vector.goal.trajectory.points.append(point)
            #Next position
            point = JointTrajectoryPoint()
            for i in range(len(self.joint_states.desired.positions)):
                point.positions.append(self.joint_states.desired.positions[i]+dq[i])
            point.time_from_start.nsecs = 2e9
            point.time_from_start.secs = 0
            joy_traj_vector.goal.trajectory.points.append(point)
            self.pub.publish(joy_traj_vector)

    # Must have __init__(self) function for a class
    # similar to a C++ class constructor.
    def __init__(self):
        # Create a publisher for turtle commands
        self.pub = rospy.Publisher('follow_joint_trajectory/goal', FollowJointTrajectoryActionGoal, queue_size=10)

        # Initialize message variables.

        # Create a subscriber for color msg
        rospy.Subscriber("empty", Empty, self.empty_callback)
        rospy.Subscriber("joy", Joy, self.joy_callback)
        rospy.Subscriber("state", JointTrajectoryControllerState, self.state_callback)
        rate = rospy.Rate(1) # 1hz
        # Main while loop.
        #while not rospy.is_shutdown():
        rate.sleep()
        # Set the message to publish as command.
        self.traj_vector = FollowJointTrajectoryActionGoal()
        # Current ROS time stamp
        h = Header()
        h.stamp = rospy.Time.now()
        self.traj_vector.header = h
        self.traj_vector.goal.trajectory.joint_names.append('arm_1_joint');
        self.traj_vector.goal.trajectory.joint_names.append('arm_2_joint');
        self.traj_vector.goal.trajectory.joint_names.append('arm_3_joint');
        self.traj_vector.goal.trajectory.joint_names.append('arm_4_joint');
        self.traj_vector.goal.trajectory.joint_names.append('arm_5_joint');
        self.traj_vector.goal.trajectory.joint_names.append('arm_6_joint');
        self.traj_vector.goal_id.stamp = h.stamp
        self.traj_vector.goal_id.id = 'rosPathPlanner' + str(h.stamp.secs)
        h2 = Header()
        h2.stamp.secs = h.stamp.secs +1
        h2.stamp.nsecs = h.stamp.nsecs + 3e8
        self.traj_vector.goal.trajectory.header.stamp = h2.stamp
        # From Octave
        octave = Octave()
        octave.eval('pkg load all')
        octave.addpath('~/robotics_lab_ws/src/rob_lab/matlab')
        q = octave.predefined_path()

        iter = 0.0
        for column in q.T:
            point = JointTrajectoryPoint()
            iter +=1
            for q in column:
                point.positions.append(q)
                point.time_from_start.nsecs = 0
                point.time_from_start.secs = iter*10
            self.traj_vector.goal.trajectory.points.append(point)
	print 'All systems go!'
        self.pub.publish(self.traj_vector)
        octave.exit()
        #print 'All systems go!'
        #while not rospy.is_shutdown():
        #    rate.sleep()
        #    if self.joy_on:
        #        self.joy_move_robot()



if __name__ == '__main__':
    # Initialize the node and name it.
    rospy.init_node('rosPathPlanner')
    # Go to class functions that do all the heavy lifting.
    # Do error checking.
    try:
        ne = PathPlanner()
    except rospy.ROSInterruptException:
        pass
