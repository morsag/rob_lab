import os
import rospy
import rospkg

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtGui import QWidget
from std_msgs.msg import Float64
from PyQt4.QtCore import SIGNAL, QTimer
from control_msgs.msg import FollowJointTrajectoryActionGoal
from control_msgs.msg import JointTrajectoryControllerState
from trajectory_msgs.msg import JointTrajectoryPoint
from std_msgs.msg import Empty
from std_msgs.msg import Header
from oct2py import Oct2Py as Octave
from sensor_msgs.msg import Joy
import numpy as np

class OR_LE_2_Planning(Plugin):

    def __init__(self, context):
        super(OR_LE_2_Planning, self).__init__(context)
        # Give QObjects reasonable names
        self.setObjectName('OR_LE_2_Planning')

        # Process standalone plugin command-line arguments
        from argparse import ArgumentParser
        parser = ArgumentParser()
        # Add argument(s) to the parser.
        parser.add_argument("-q", "--quiet", action="store_true",
                      dest="quiet",
                      help="Put plugin in silent mode")
        args, unknowns = parser.parse_known_args(context.argv())
        if not args.quiet:
            print 'arguments: ', args
            print 'unknowns: ', unknowns

        # Create QWidget
        self._widget = QWidget()
        # Get path to UI file which should be in the "resource" folder of this package
        ui_file = os.path.join(rospkg.RosPack().get_path('rob_lab'), 'resource', 'OR_LE_2_Planning.ui')
        # Extend the widget with all attributes and children from UI file
        loadUi(ui_file, self._widget)
        # Give QObjects reasonable names
        self._widget.setObjectName('OR_LE_2_PlanningUi')
        # Show _widget.windowTitle on left-top of each plugin (when 
        # it's set in _widget). This is useful when you open multiple 
        # plugins at once. Also if you open multiple instances of your 
        # plugin at once, these lines add number to make it easy to 
        # tell from pane to pane.
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))
        # Add widget to the user interface
        context.add_widget(self._widget)
        # GO TO BUTTONS
        self._gotoButtons=('pushButtonW1','pushButtonW2','pushButtonW3','pushButtonHOME')
        for gotoButtonName in self._gotoButtons:
	    pushButton = self._widget.findChild(QWidget,gotoButtonName)
            pushButton.clicked.connect(self.gotoButton_click_clbck(gotoButtonName))
        # CARTESIAN MOVE BUTTONS
        self._cartesianButtons = ('pushButtonDXNEG','pushButtonDXPOZ','pushButtonDYNEG','pushButtonDYPOZ','pushButtonDZPOZ','pushButtonDZNEG')
        #self._cartesianButtonsPublishers = {}
        for pushButtonName in self._cartesianButtons:
            pushButton = self._widget.findChild(QWidget,pushButtonName)
            pushButton.clicked.connect(self.cartesianButton_click_clbck(pushButtonName))
            #self._cartesianButtonsPublishers[pushButtonName]=rospy.Publisher(pushButtonName, Float64)
        self._cartesianLCDs = ('lcdNumber_XPOS','lcdNumber_YPOS','lcdNumber_ZPOS','lcdNumber_R13','lcdNumber_R23','lcdNumber_R33')
        self._jointLCDs = ('lcdNumber_Q1','lcdNumber_Q2','lcdNumber_Q3','lcdNumber_Q4','lcdNumber_Q5','lcdNumber_Q6')
        #self._subscriber_temp = rospy.Subscriber('/pushButtonDXNEG',Float64,self.tempROScallback)
        self.connect(self, SIGNAL("UpdatePosePosition"),self.update_LCD)
        #EXECUTE SCRIPT BUTTON 
        self._widget.pushButtonSCRIPT.clicked.connect(self.scriptButton_click_clbck)
        # START OCTAVE SESSION
        self._octave = Octave()
	self._octave.eval('pkg load all')
	self._octave.addpath('~/robotics_lab_ws_indigo/src/rob_lab/matlab')
	self._octaveRefresh = Octave()
	self._octaveRefresh.eval('pkg load all')
	self._octaveRefresh.addpath('~/robotics_lab_ws_indigo/src/rob_lab/matlab')
	# Create a publisher for Schunk LWA4 Powerball commands
        self.pub = rospy.Publisher('arm_controller/follow_joint_trajectory/goal', FollowJointTrajectoryActionGoal, queue_size=10)
	self.traj_vector = FollowJointTrajectoryActionGoal()
	# Create a subscriber to Controller JointTrajectoryControllerState
        self.sub = rospy.Subscriber('arm_controller/state',JointTrajectoryControllerState,self.robotStateROScallback)
	#GUI refresh timer
	self.refreshTimer = QTimer()
	self.refreshTimer.timeout.connect(self.refreshTimer_clbck)
	self.refreshTimer.start(200)
	  
    def refreshTimer_clbck(self):
	    #print self._robotData.actual.positions
	    self.emit(SIGNAL("UpdatePosePosition"))
	    self.refreshTimer.start(200)

    #Here we update the GUI with data received from ROS
    def update_LCD(self):
        #LCD_dictionary = dict(zip(self._cartesianLCDs,(1,2,3)))#1,2,3 values to pass to GUI
        #for LCDName in self._cartesianLCDs:
        #    self._widget.findChild(QWidget,LCDName).setProperty("value", LCD_dictionary[LCDName])
	LCD_dictionary = dict(zip(self._jointLCDs,self._robotData.actual.positions))#1,2,3 values to pass to GUI
        #LCD_dictionary = dict(zip(self._jointLCDs,(1.0,2,3,4,5,6)))
        for LCDName in self._jointLCDs:
            self._widget.findChild(QWidget,LCDName).setProperty("value", 180*LCD_dictionary[LCDName]/np.pi)
        q = np.matrix(self._robotData.actual.positions).transpose()
        q[2][0]=-q[2][0]
	w = self._octaveRefresh.dk_schunk_lwa4p(q).tolist()
	LCD_dictionary=dict(zip(self._cartesianLCDs,w))
	for LCDName in self._cartesianLCDs:
            self._widget.findChild(QWidget,LCDName).setProperty("value", LCD_dictionary[LCDName][0])
            #print LCD_dictionary[LCDName][0]
    #Here we get new Pose Position info from ROS and pass it on to the GUI
    def robotStateROScallback(self,data):
	self._robotData = data
        #self.emit(SIGNAL("UpdatePosePosition"), 'lcdNumber_XPOS', data)
        
    def scriptButton_click_clbck(self):
	path = self._widget.lineEditPath.text()
	print path
	self._octave.addpath(path)
	
	self.fill_trajectory_goal_header()
	# From Octave
	q = self._octave.execute_script()
	#Fill in Qs
	self.traj_vector.goal.trajectory.points[:]=[]
	iter = 0.0
	for column in q.T:
	    point = JointTrajectoryPoint()
	    iter +=1
	    for q in column:
		point.positions.append(q)
		point.time_from_start.nsecs = 0
		point.time_from_start.secs = iter*10
	    self.traj_vector.goal.trajectory.points.append(point)
	print 'All systems go!'
	self.pub.publish(self.traj_vector)
    def gotoButton_click_clbck(self,name):
	def executeGOTO_click():
	    #HERE we do all the interesting stuff
	    gotoButtonDictionary = dict(zip(self._gotoButtons,(1,2,3,4)))    
	    # Here we create and publish a new setpoint in cartesian space
            # First we need to fill all the information of the new Goal value
	    # Current ROS time stamp
	    self.fill_trajectory_goal_header()
	    # From Octave
	    q = self._octave.predefined_point(gotoButtonDictionary[name])
	    #Fill in Qs
	    self.traj_vector.goal.trajectory.points[:]=[]
	    iter = 0.0
	    for column in q.T:
		point = JointTrajectoryPoint()
		iter +=1
		for q in column:
		    point.positions.append(q)
		    point.time_from_start.nsecs = 0
		    point.time_from_start.secs = iter*10
		self.traj_vector.goal.trajectory.points.append(point)
	    print 'All systems go!'
	    self.pub.publish(self.traj_vector)
	return executeGOTO_click

    #This is a function factory for CARTESIAN BUTTONS
    def cartesianButton_click_clbck(self,name):
        def pushButtonDXNEG_click_clbck():
            # Here we create and publish a new setpoint in cartesian space
            # First we need to fill all the information of the new Goal value
	    # Current ROS time stamp
	    self.fill_trajectory_goal_header()
	    # From Octave
	    q = self._octave.predefined_path()
	    #Fill in Qs
	    iter = 0.0
	    for column in q.T:
		point = JointTrajectoryPoint()
		iter +=1
		for q in column:
		    point.positions.append(q)
		    point.time_from_start.nsecs = 0
		    point.time_from_start.secs = iter*10
		self.traj_vector.goal.trajectory.points.append(point)
	    print 'All systems go!'
	    self.pub.publish(self.traj_vector)
            #self._cartesianButtonsPublishers[name].publish(Float64(10))
        return pushButtonDXNEG_click_clbck
      
    def fill_trajectory_goal_header(self):
	h = Header()
	h.stamp = rospy.Time.now()
	self.traj_vector.header = h
	#Joint names
	self.traj_vector.goal.trajectory.joint_names.append('arm_1_joint');
	self.traj_vector.goal.trajectory.joint_names.append('arm_2_joint');
	self.traj_vector.goal.trajectory.joint_names.append('arm_3_joint');
	self.traj_vector.goal.trajectory.joint_names.append('arm_4_joint');
	self.traj_vector.goal.trajectory.joint_names.append('arm_5_joint');
	self.traj_vector.goal.trajectory.joint_names.append('arm_6_joint');
	self.traj_vector.goal_id.stamp = h.stamp
	self.traj_vector.goal_id.id = 'rosPathPlanner' + str(h.stamp.secs)
	h2 = Header()
	h2.stamp.secs = h.stamp.secs +1
	h2.stamp.nsecs = h.stamp.nsecs + 3e8
	self.traj_vector.goal.trajectory.header.stamp = h2.stamp
	

    def shutdown_plugin(self):
        # TODO unregister all publishers here
        self._octave.exit()
        self._octaveRefresh.exit()
        pass

    def save_settings(self, plugin_settings, instance_settings):
        # TODO save intrinsic configuration, usually using:
        # instance_settings.set_value(k, v)
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # TODO restore intrinsic configuration, usually using:
        # v = instance_settings.value(k)
        pass

    #def trigger_configuration(self):
        # Comment in to signal that the plugin has a way to configure
        # This will enable a setting button (gear icon) in each dock widget title bar
        # Usually used to open a modal configuration dialog
