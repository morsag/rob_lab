Instructions for setting up the 2nd laboratory practical for Fundamentals of robotics (Trajectory planning). The instructions currently apply to Ubuntu 14.04 Trusty and ROS Indigo.

# Installing the software #

## The CAN transceiver driver ##

The CAN driver depends on the libpopt library, which has to be installed beforehand:

```
#!shell

sudo apt-get install libpopt-dev
```

Download a recent version of the driver from the [PEAK-System website](http://www.peak-system.com/fileadmin/media/linux/index.htm). Version 7.15.2 was used at the time these notes were written. Unpack the archive, switch to the unpacked directory and build the driver using the following commands:

```
#!shell

make NET=NO_NETDEV_SUPPORT
sudo make install
```

If everything went well, the red diagnostic LED on the CAN transceiver should start blinking (TODO: check!) when you plug in the the transceiver.

## EIGEN ##
You will need to find the latest version of Eigen on http://eigen.tuxfamily.org/index.php?title=Main_Page, download and follow the instructions in install file to install the software. (TODO: check if this is actually necessary)

## ROS packages ##

You'll need ROS Indigo. Create the Catkin workspace and source it's setup.bash. Don't forget to update your .bashrc file. Make sure that the roscd command takes you to the correct workspace. Clone the following packages and checkout the correct branches.

```
#!shell

git clone https://github.com/ipa320/schunk_modular_robotics.git 
cd schunk_modular_robotics
git fetch
git checkout indigo_release_candidate
cd ..

```
### You will need to download following packages ###

```
#!shell
sudo apt-get install ros-indigo-libntcan
sudo apt-get install ros-indigo-libpcan
sudo apt-get install ros-indigo-cob-sound
sudo apt-get install ros-indigo-brics-actuator
```

### Warning since Kinetic the previous packages have to be downloaded from source ###
```
#!shell
git clone https://github.com/ipa320/cob_extern.git
```

### You will need to perform the following manual change in this package: ###

in schunk_modular_robotics/schunk_description/urdf/lwa4p/lwa4p.transmission.xacro change the contents of all hardwareInterface nodes to EffortJointInterface  and add a <hardwareInterface>EffortJointInterface</hardwareInterface> node under every actuator node

```
#!shell

git clone https://github.com/ipa320/cob_common.git
cd cob_commmon
git fetch
git checkout hydro_release_candidate
cd ..
```


```
#!shell

git clone https://github.com/ipa320/ipa_canopen.git
cd ipa_canopen
git fetch
git checkout indigo_release_candidate
cd ..
```
In ipa_canopen_ros/src/ipa_canopen_ros.cpp,  lines 517-529 should be commented.
```
#!shell

git clone https://github.com/ipa320/schunk_robots.git
cd schunk_robots
git fetch
git checkout indigo_dev
cd ..
```

```
#!shell

git clone https://github.com/ipa320/cob_command_tools.git
cd cob_command_tools
git fetch
git checkout hydro_release_candidate
cd ..
```

In the file cob_command_tools/cob_teleop/ros/src/cob_teleop.cpp comment out lines 871-877 and 880

```
#!shell
git clone https://github.com/catkin/catkin_simple.git

```

```
#!shell

git clone https://github.com/ipa320/cob_control.git
cd cob_control
git fetch
git checkout hydro_release_candidate
cd ..
```

```
#!shell

git clone https://github.com/ipa320/cob_control.git
roscd cob_common
git fetch
git checkout hydro_release_candidate

```
You will need to comment out the line 7 (#find_package(eigen REQUIRED)) in cmake file of the package cob_trajectory_controller.

```
#!shell

git clone https://bitbucket.org/morsag/rob_lab.git

```
In order to use the pyqt interface you will need to download and install:
```
#!shell

sudo apt-get install python-kde4

```
### Making ###
First you need to make a single package cob_srvs first

```
#!shell

catkin_make --pkg cob_srvs

```
After that, just catkin make your workspace

```
#!shell

catkin_make

```
### Running ###
First launch everything with:
```
#!shell

roslaunch rob_lab lwa4p_nase.launch

```

After that initialize the robot with

```
#!shell

rosservice call /arm_controller/init

```

You should be able to control the robot through joystick.