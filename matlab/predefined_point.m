function Q = predefined_point(iter)
w_points=[
-800,-300,240,-1,0,0;
-800,0,440,-1,0,0;
-800,300,240,-1,0,0];

Q0 = [0.0, 0.0, 0.0, 0.0,0.0,0.0];
[n,m]=size(w_points);
points=[];
Q0 = [0.0, 0.0, 0.0, 0.0,0.0,0.0];

if (iter>0) && (iter<4)
  q=ik_lwa4p_closest(w_points(iter,:)',Q0);
else
  q=Q0';
end
q(3)=-q(3)
Q = q
Q(1,:)=Q(1,:)+pi/2

