function q = taylor_path_lwa4p(w1,w2,tol)

q = taylor_path(w1,w2,tol,@dk_schunk_lwa4p,@ik_lwa4p_closest,0);
[n,m] = size(q);
for i=2:m,
    q(:,i)=ik_lwa4p_closest(dk_schunk_lwa4p(q(:,i)),q(:,i-1));
end;
