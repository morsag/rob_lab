function h = plot_path(q,dkf)
% PLOT_PATH Plots a manipulator path in 3D.
%  H = PLOT_PATH(Q,DKF) Plots the path, given as a sequence of joint
%  variable vectors Q (each colum corresponds to one joint variable
%  configuration) in cartesian space. Cartesian coordinates are obtained by
%  passing each column of Q to the direct kinematics function DKF.

% Schunk kinematic parameters
d = [205, 0, 0, 316, 0, 318.3];
a = [0, 369.2, 0, 0, 0, 0];

% Interpolate path in joint coordinates with 0.1deg resolution (for joint
% with largest path).
q_int = q(:,1);
for k = 1:size(q,2)-1
    dq_max_k = norm(wrapToPi(q(:,k)-q(:,k+1)),inf);
    n = ceil(dq_max_k/(0.1*pi/180));
    if n > 1
        q_int = [q_int,zeros(size(q,1),n)];
        for j = 1:length(q(:,k))
            q_int(j,end-n+1:end) = linspace(q(j,k),q(j,k+1),n);
        end
    else
        q_int = [q_int, q(:,k+1)];
    end
end
%q_int=q;
w = zeros(6,size(q_int,2));
for k = 1:size(q_int,2)
    w(:,k) = dkf(q_int(:,k));
end

h = plot3(w(1,:),w(2,:),w(3,:),'-*',...
    [w(1,1),w(1,end)],[w(2,1),w(2,end)],[w(3,1),w(3,end)],'k-o');
view(90,0);
