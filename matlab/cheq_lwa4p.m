function q = cheq_lwa4p(qin)
% TEST_Q Test LWA4P joint vectors for feasibility.
%   Q = TEST_Q_LWA4P(QIN) For every column of QIN, checks if its values are
%   within the feasible range of Schunk LWA4 Powerball joints. Feasible
%   joint vectors are copied to Q, infeasible vectors are discarded.

if (size(qin,1) ~= 6)
    error(['Joint variable vector should have 6 rows but found', mat2str(length(qin)),'!'])
end

q = qin;

q_min = [-170,-110,-155,-170,-140,-170]' * pi / 180;
q_max = -q_min;
for k = size(q,2):-1:1
    if ~all( (q(:,k) >= q_min) & (q(:,k) <= q_max) )

        q(:,k) = [];

    end
end

