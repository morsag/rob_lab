function [planned_points]=spline_3(points)

DT1=max(abs(points(:,2)-points(:,1)))/0.1;

A1=[0 0 1;
    DT1^2 DT1 1;
    0 1 0];
IA1= inv(A1);
P1 = zeros(3,6);

for i=1:6,
    Q = [points(i,1);points(i,2);0];
    P1(:,i)=IA1*Q;
end;

DT2=max(abs(points(:,3)-points(:,2)))/0.1;
A2=[0 0 0 1;
    DT2^3 DT2^2 DT2 1;
    0 0 1 0;
    3*DT2^2 2*DT2 1 0];
IA2= inv(A2); 
P2 = zeros(4,6);

for i=1:6,
    Q = [points(i,2); points(i,3); 2*P1(1,i)*DT1+P1(2,i); 0];
    P2(:,i)=IA2*Q;
end;

planned_points = [];
dt1=linspace(0,DT1,10);
for i = 1:10
planned_points=[planned_points,[polyval(P1(:,1),dt1(i));polyval(P1(:,2),dt1(i));polyval(P1(:,3),dt1(i));polyval(P1(:,4),dt1(i));polyval(P1(:,5),dt1(i));polyval(P1(:,6),dt1(i));]];
end;

dt2=linspace(0,DT2,10);
for i = 2:10
planned_points=[planned_points,[polyval(P2(:,1),dt2(i));polyval(P2(:,2),dt2(i));polyval(P2(:,3),dt2(i));polyval(P2(:,4),dt2(i));polyval(P2(:,5),dt2(i));polyval(P2(:,6),dt2(i));]];
end;