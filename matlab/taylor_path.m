function q = taylor_path(w1,w2,tol,dkf,ikf,nrec)
% TAYLOR_PATH Compute a robotic arm path using the Taylor method.
%  Q = TAYLOR_PATH(W1,W2,TOL,DKF,IKF) Perform the Taylor approximation
%  method with tolerance TOL between W1 and W2.

q = [];
q1 = ikf(w1,zeros(6,1));
q2 = ikf(w2,q1);

if (~isempty(q1) && ~isempty(q2))

    q = [q1,q2];
    
    qm = (q(:,1)+q(:,2))/2;
    wm = dkf(qm);
    wM = (w1+w2)/2;

    if ((norm(wm(1:3)-wM(1:3),2) > tol) && nrec < 100)
        q = taylor_path(w1,wM,tol,dkf,ikf,nrec+1);
        q = [q(:,1:end-1), taylor_path(wM,w2,tol,dkf,ikf,nrec+1)];
    end
end