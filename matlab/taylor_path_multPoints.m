function tpath = taylor_path_multPoints(path,tol)
[n,m] = size(path);
tpath = [];
tpath = taylor_path_lwa4p(dk_schunk_lwa4p(path(:,1)),dk_schunk_lwa4p(path(:,2)),tol);
for i=3:m,
    temp=taylor_path_lwa4p(dk_schunk_lwa4p(path(:,i-1)),dk_schunk_lwa4p(path(:,i)),tol);
    temp(:,1)=[];
    tpath = [tpath,temp];
end;